
# react-native-tizen-watch

## Getting started

`$ npm install react-native-tizen-watch --save`

### Mostly automatic installation

`$ react-native link react-native-tizen-watch`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-tizen-watch` and add `RNTizenWatch.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNTizenWatch.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNTizenWatchPackage;` to the imports at the top of the file
  - Add `new RNTizenWatchPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-tizen-watch'
  	project(':react-native-tizen-watch').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-tizen-watch/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-tizen-watch')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNTizenWatch.sln` in `node_modules/react-native-tizen-watch/windows/RNTizenWatch.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Tizen.Watch.RNTizenWatch;` to the usings at the top of the file
  - Add `new RNTizenWatchPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNTizenWatch from 'react-native-tizen-watch';

// TODO: What to do with the module?
RNTizenWatch;
```
  